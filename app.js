// Configures the core http server
"use strict";

var express = require("express");
var http = require("http");

module.exports = function(options, imports, register) {

  var debugApp = imports.debug('http:app');
  var debugServer = imports.debug('http:server');
  debugApp("start");

  // Create app and add to server
  var app = express();
  var server = http.createServer(app);

  // Create basic api
  var api = {
    getExpress: function () {
      return express;
    },
    on: app.on,
    emit: app.emit,
    getApp: function(){
      return app;
    },
    Router: express.Router.bind(express),
    getNewRouter: function(){
      return express.Router();
    },
    static: express.static.bind(express),
    start: function(){
      startListening(options.port, options.host);
    }
  };

  // if a hook early in the chain doesn't have functions, the server will throw an error
  // TODO: figure out how to avoid the error
  var hookNames = [
    // "Start",
    "Setup",
    "Security",
    "",
    "Apps",
    "Static",
    "Error"
  ];

  function setupHook(hookName){
    debugApp("set up hook server: %s", hookName);
    var hookServer = express();
    app.use(hookServer);
    api["use" + hookName] = hookServer.use.bind(hookServer);
  }

  // Set up hooks to add middleware and routes in a particular order
  hookNames.forEach(function (name) {
    setupHook(name);
  });

  // Set final port on the api and start listening
  function startListening(port, host) {
    api.getPort = function () {
      return port;
    };
    api.getHost = function () {
      return host;
    };

    if (host) {
      server.listen(port, host, listenCallback);
    } else {
      server.listen(port, listenCallback);
    }

    function listenCallback(err){
      if (err) {
        debugServer("error starting server");
        process.exit(1);
      } else {
        debugServer("express server listening at http://" + host + ":" + port);
      }
    }

  }




  debugApp("register express and http");
  register(null, {
    "onDestruct": function (callback) {
      app.close();
      app.on("close", callback);
    },
    "express": api,
    "http": {
      getServer: function () {
        return server;
      }
    }
  });

};

